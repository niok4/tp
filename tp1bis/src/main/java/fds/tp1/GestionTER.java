package fds.tp1;
import java.util.ArrayList;
import java.util.*;
public class GestionTER {
	private ArrayList<Groupe> listeGrp;
	private ArrayList<Sujet> listeSujet;
	private int passe=1;
	
	Enseignant respTER= new Enseignant();
	
	public void setResponsable(Enseignant resp) {
		this.respTER= resp;
	}
	
	public void ajoutGroupe(Groupe g) throws sizeException{
		if (listeGrp.size()>=5) {
			throw new sizeException("Taille du groupe maximale atteinte.");
		}
		this.listeGrp.add(g);
	}
	public void ajoutSujet(Sujet s){
		this.listeSujet.add(s);
	}
	public int getNbrGrp() {
		return listeGrp.size();
	}
	public int getNbrSujet() {
		return listeSujet.size();
	}
	public void setPasse(int passe) {
		this.passe = passe;
	}

	public void affectationSujet() throws nbrGrpSupANbrSujetException{
		if(listeGrp.size()>listeSujet.size()) {
			throw new nbrGrpSupANbrSujetException("Pas assez de sujet ce nombre de groupe.");
		}
		int taille=getNbrGrp();
			for(int i=0;i<taille;i++) {
				listeGrp.get(i).getVoeux();
				for(int j=0;j<5;j++) {
					if (listeGrp.get(i).getVoeux().get(j).getAffecte() == false){
						listeGrp.get(i).getVoeux().get(j).setAffecte(true);
						listeGrp.get(i).setSujetAffecte(listeGrp.get(i).getVoeux().get(j));	
					}
					if (passe==1) {
						if(listeGrp.get(i).getSujetAffecte()==null) {
								listeGrp.get(i).resetVoeux();
						}
					} else {
						while (listeGrp.get(i).getSujetAffecte()==null) {
								Collections.shuffle(listeSujet);
								Sujet sujetRandom = new Sujet();
								sujetRandom=listeSujet.get(0);
								if (sujetRandom.getAffecte()==false){
									listeGrp.get(i).setSujetAffecte(sujetRandom);									
								}
						}				
					}
				}	
			}
			setPasse(2);
	}
		
	public GestionTER(int nbrGrp){
		 listeGrp=new ArrayList<Groupe>(nbrGrp);
	}
	
	
	public static void main(String[] Args){
	}

}
